using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;


var builder = WebApplication.CreateBuilder(args);


#region

// var config = new ConfigurationBuilder()
//     .AddJsonFile("ocelot.json", optional: false, reloadOnChange: true)
//     .Build();
// builder.Services.AddOcelot(config);

builder.Configuration.AddJsonFile("ocelot.json", optional: false, reloadOnChange: true);

#endregion


builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer("auth1", options =>
    {
        options.Authority = "http://localhost:6001";
        options.Audience = "api1";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters { ValidateAudience = false };
    })
    .AddJwtBearer("auth2", options =>
    {
        options.Authority = "http://localhost:6001";
        options.Audience = "api2";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters { ValidateAudience = false };
    });

builder.Services.AddOcelot();

var app = builder.Build();
app.MapGet("/", () => "Hello World!");

app.UseOcelot().Wait();

app.UseAuthorization();
app.Run();