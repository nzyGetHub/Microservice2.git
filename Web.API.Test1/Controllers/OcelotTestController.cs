﻿using Microsoft.AspNetCore.Mvc;

namespace Web.API.Test1.Controllers;

[ApiController]
[Route("[controller]")]
public class OcelotTestController : ControllerBase
{
    [HttpGet("GetOcelotTest")]
    public IActionResult GetOcelotTest()
    {
        return Ok("DateTime：" + DateTime.Now + "--Port：5001");
    }
}