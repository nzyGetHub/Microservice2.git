﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;

namespace Ids4.Server;

public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
{
    public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
    {
        // 根据username和password查询用户是否存在
        var user = UserData.GetListUsers().FirstOrDefault(p => p.Username == context.UserName && p.Password == context.Password);
        if (user != null)
        {
            // 返回Id，为下一步获取角色权限做准备
            context.Result = new GrantValidationResult(user.Id, OidcConstants.AuthenticationMethods.Password);
        }
        else
        {
            context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid credentials");
        }
    }
}
