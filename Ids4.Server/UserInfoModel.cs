﻿namespace Ids4.Server;

public class UserInfoModel
{
    public string Id { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public string Role { get; set; }
}