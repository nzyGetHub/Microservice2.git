﻿using IdentityServer4.Models;
using static IdentityServer4.IdentityServerConstants;

namespace Ids4.Server;

public class Config
{
    public static IEnumerable<IdentityResource> IdentityResources =>
        new List<IdentityResource>
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile()
        };

    public static IEnumerable<ApiScope> ApiScopes =>
        new ApiScope[]
        {
            //new ApiScope("api1", "My Api1"),
            new ApiScope("api2", "My Api2")
        };

    public static IEnumerable<Client> Clients =>
        new List<Client>
        {
            // new Client
            // {
            //     ClientId = "client_api1",
            //
            //     // 没有交互式用户，使用 clientid/secret 进行身份验证
            //     AllowedGrantTypes = GrantTypes.ClientCredentials,
            //
            //     // 用于身份验证的密钥
            //     ClientSecrets =
            //     {
            //         new Secret("secret".Sha256()) //secret加密密钥 Sha256加密方式
            //     },
            //
            //     // 客户端有权访问的范围
            //     AllowedScopes =
            //     {
            //         "api1",
            //         //StandardScopes.OfflineAccess
            //     },
            //     AllowOfflineAccess = true,
            //     AccessTokenLifetime = 3600, //过期时间，默认3600秒
            //
            //     Claims = new List<ClientClaim>()
            //     {
            //         new ClientClaim()
            //         {
            //             Type = "Test",
            //             Value = "333@qq.com"
            //         }
            //         // new ClientClaim()
            //         // {
            //         //     Type = "Email",
            //         //     Value = "3311@163.com"
            //         // }
            //     },
            //
            //     // RefreshTokenUsage = TokenUsage.ReUse,
            //     // RefreshTokenExpiration = TokenExpiration.Sliding,
            //     // SlidingRefreshTokenLifetime = 2592000,
            // },
            new Client
            {
                ClientId = "client_api2",
                // 要使用RefreshToken时，必须要把AllowedGrantTypes设置为授权代码、混合和资源所有者密码凭证流
                AllowedGrantTypes = GrantTypes.ResourceOwnerPassword, //GrantTypes.ClientCredentials,
                ClientSecrets =
                {
                    new Secret("secret".Sha256()) //secret加密密钥 Sha256加密方式
                },
                AllowedScopes =
                {
                    "api2",
                    StandardScopes.OfflineAccess,
                },
                // 刷新Token时RefreshToken保持不变
                RefreshTokenUsage = TokenUsage.ReUse,
                RefreshTokenExpiration = TokenExpiration.Sliding,
                // RefreshToken过期时间
                SlidingRefreshTokenLifetime = 3600,
                // 明确授权请求刷新令牌
                AllowOfflineAccess = true,
                // TOken过期时间
                AccessTokenLifetime = 60,
            }
        };
}