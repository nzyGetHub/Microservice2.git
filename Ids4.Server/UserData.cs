﻿namespace Ids4.Server;

public static class UserData
{
    /// <summary>
    /// 模拟数据库存储的用户信息
    /// </summary>
    /// <returns></returns>
    public static List<UserInfoModel> GetListUsers()
    {
        return new List<UserInfoModel>
        {
            new UserInfoModel
            {
                Id = "1",
                Username = "zhangsan",
                Password = "123456",
                Role = "admin"
            },
            new UserInfoModel
            {
                Id = "2",
                Username = "lisi",
                Password = "123456",
                Role = "Test1,Test2"
            },
            new UserInfoModel
            {
                Id = "2",
                Username = "lisi",
                Password = "123456",
                Role = "Test2"
            }
        };
    }
}