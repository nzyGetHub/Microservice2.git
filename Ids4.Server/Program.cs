using IdentityServer4.Services;
using Ids4.Server;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTransient<IRefreshTokenService, RefreshTokenService>();
builder.Services.AddIdentityServer()
    .AddDeveloperSigningCredential()
    .AddInMemoryApiScopes(Config.ApiScopes)
    .AddInMemoryClients(Config.Clients)
    .AddInMemoryIdentityResources(Config.IdentityResources)
    .AddProfileService<ProfileService>()
    .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>();
    //.AddTestUsers(Config.GetListUsers());
    
var app = builder.Build();

app.MapGet("/", () => "Hello World!");
app.UseIdentityServer();
app.Run();