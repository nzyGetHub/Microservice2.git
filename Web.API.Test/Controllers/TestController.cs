﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Test.Controllers;

[ApiController]
[Route("[controller]")]
public class TestController : ControllerBase
{
    [Authorize]
    [HttpGet("GetAuthTest")]
    public IActionResult GetAuthTest()
    {
        return Ok("授权信息");
    }

    [Authorize("AdminPolicy")]
    [HttpGet("GetAdminAuthTest")]
    public IActionResult GetAdminAuthTest()
    {
        return Ok("只允许角色为admin的访问");
    }
}